//
//  Message.swift
//  ChatAPI
//
//  Created by Daniel Szlaski on 07/05/2019.
//  Copyright © 2019 Daniel Szlaski. All rights reserved.
//
import Foundation
import UIKit
import MessageKit

struct Message {
    let member: Member
    let text: String
    let messageId: String
}

extension Message: MessageType {
  
    var sender: SenderType {
        return Sender(senderId: member.name, displayName: member.name)
    }
   
    var sentDate: Date {
        return Date()
    }
    
    var kind: MessageKind {
        return .text(text)
    }
}

struct Member {
    var name: String
    let color: UIColor
}

